import logo from "./logo.svg";
import "./App.css";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";

function App() {
  const defaultForm = { name: "", price: "", qty: "", total: "" };
  const addForm = { name: "", price: "", qty: "1", total: "" };
  const [forms, setForms] = useState([defaultForm]);
  const [grandTotal, setGrandTotal] = useState(0);

  useEffect(() => {
    handleGrandTotal();
  }, [forms]);

  const handlePriceChange = (index, price) => {
    const newForms = [...forms];
    newForms[index] = {
      ...newForms[index],
      price,
      total: price * newForms[index].qty,
    };
    setForms(newForms);
    handleGrandTotal();
  };

  const handleQtyChange = (index, qty) => {
    if (isNaN(qty) || qty < 1) {
      return new Swal({title : 'Ooops', text:'Quantity barang tidak boleh kurang dari 1', icon: 'error'})
    }
    const newForms = [...forms];
    newForms[index] = {
      ...newForms[index],
      qty,
      total: newForms[index].price * qty,
    };
    setForms(newForms);
    handleGrandTotal();
  };

  const handleNameChange = (index, name) => {
    const newForms = [...forms];
    newForms[index] = {
      ...newForms[index],
      name,
    };
    setForms(newForms);
  };

  const handleGrandTotal = () => {
    let calculate = 0;
    forms.map((form) => {
      calculate += form.total;
    });
    setGrandTotal(calculate);
  };

  const handleAddForm = () => {
    setForms([...forms, addForm]);
  };

  const handleRemoveForm = (index) => {
    const newForms = [...forms];
    newForms.splice(index, 1);
    setForms(newForms);
  };

  return (
    <div className="flex justify-center">
      <div className="m-8">
        <div>
          <button className="btn btn-accent btn-sm" onClick={handleAddForm}>
            New
          </button>
        </div>
        <div id="form-section">
          {forms.map((form, index) => (
            <div key={index} className="flex gap-4 content-center">
              <div className="form-control">
                <label className="label">
                  <span className="label-text">Nama Produk</span>
                </label>
                <input
                  className="input input-bordered"
                  placeholder="Product Name"
                  value={form.name}
                  onChange={(e) => handleNameChange(index, e.target.value)}
                />
              </div>
              <div className="form-control">
                <label className="label">
                  <span className="label-text">Harga</span>
                </label>
                <input
                  className="input input-bordered"
                  placeholder="Product Price"
                  value={form.price}
                  onChange={(e) =>
                    handlePriceChange(index, parseFloat(e.target.value))
                  }
                />
              </div>
              <div className="form-control">
                <label className="label">
                  <span className="label-text">Jumlah</span>
                </label>
                <input
                  className="input input-bordered"
                  placeholder="Qty"
                  value={form.qty}
                  onChange={(e) =>
                    handleQtyChange(index, parseInt(e.target.value))
                  }
                />
              </div>
              <div className="form-control">
                <label className="label">
                  <span className="label-text">Total</span>
                </label>
                <input
                  className="input input-bordered"
                  placeholder="Total"
                  value={form.total}
                  readOnly={true}
                />
              </div>
              {index !== 0 && (
                <div className="flex">
                  <button className="mt-auto mb-2 btn btn-sm btn-error" onClick={() => handleRemoveForm(index)}>Delete</button>
                </div>
              )}
            </div>
          ))}
        </div>
        <div>
          <div className="form-control">
            <label className="label">
              <span className="label-text">Grand Total</span>
            </label>
            <input
              className="input input-bordered"
              placeholder="Grand Total"
              value={grandTotal}
              readOnly={true}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
